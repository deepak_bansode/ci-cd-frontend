import * as React from "react"

const pageStyles = {
  color: "#232129",
  padding: 100,
  fontFamily: "-apple-system, Roboto, sans-serif, serif",
}

const IndexPage = () => {
  return (
    <main style={pageStyles}>
      <h1>You can do it Deepak!</h1>
      <p>The Knowledge about a system is directly propertional to time spend on it.</p>
    </main>
  )
}

export default IndexPage

export const Head = () => <title>Home Page</title>
